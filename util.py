import discord
from discord.ext import commands
import asyncio

class Util():
    """General utility functions"""
    
    def __init__(self, client):
        self.client = client
        self.BLANK = "\u200B"

    # Embed utility functions #

    def create_embed(self, title: str, description: str):
        """Creates a simple embed"""
        embed = discord.Embed(
            title = title,
            description = description,
            color = self.get_discord_color(self.client.config["default_embed_color"])
        )

        return embed

    def add_embed_section(self, embed: discord.Embed, name: str, value: str, inline: bool = True):
        """Adds a basic section to an embed"""
        embed.add_field(
            name = name,
            value = value,
            inline = inline
        )

        return embed

    def add_embed_col_section(self, embed: discord.Embed, name: str, labels: list, values):
        """Add a section with columns to an embed (Max 3 columns)"""
        if (len(labels) != len(values)):
            embed.add_field(name = "Could not add field!", value = self.BLANK)
            return embed

        # Section name field
        embed.add_field(
            name = self.BLANK,
            value = name,
            inline = False
        )

        # Section label and value field
        for i in range(len(labels)):
            if (type(values[i]) is list):
                embed.add_field(
                    name = labels[i],
                    value = "\n".join(values[i])
                )
            else:
                embed.add_field(
                    name = labels[i],
                    value = "{}\n".format(values[i])
                )

        return embed

    def add_embed_row_section(self, embed: discord.Embed, name: str, labels: list, values: list):
        """Add a section with rows to an embed"""
        if (len(labels) != len(values)):
            embed.add_field(name = "Could not add field!", value = self.BLANK)
            return embed

        # Section name field
        embed.add_field(
            name = self.BLANK,
            value = name,
            inline = False
        )

        # Section label and value fields
        embed.add_field(
            name = "Row",
            value = "\n".join("**{}**".format(label) for label in labels)
        )
        embed.add_field(
            name = "Value",
            value = "\n".join(values)
        )

        return embed

    def add_embed_timestamp(self, embed: discord.Embed):
        """Add a basic timestamp footer to an embed"""
        return

    # Misc utility functions #

    def get_discord_color(self, rgb: list):
        """Return a discord.Color from an RGB list"""
        return discord.Color.from_rgb(
            rgb[0],
            rgb[1],
            rgb[2]
        )

    # Response utility functions #

    async def wait_for_reaction_add(self, reaction = None, user: discord.user = None):
        """Wait for reaction add"""
        def check(r: discord.Reaction, u: discord.User):
            isReaction = reaction is None or str(r.emoji) == reaction or str(r.emoji) in reaction
            isUser = user is None or u == user
            return isReaction and isUser
        
        try:
            result = await self.client.wait_for(
                "reaction_add", 
                check = check, 
                timeout = self.client.config["default_wait_time"]
            )
            return result
        except asyncio.TimeoutError:
            return None

    async def wait_for_message(self, message: discord.Message):
        def check(m: discord.Message):
            return True

        result = await self.client.wait_for("message", check = check)
        return result