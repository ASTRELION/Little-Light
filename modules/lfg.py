import discord
from discord.ext import commands
from little_light import LittleLightClient
import util
import destiny
import typing
import json

def setup(client):
    client.add_cog(LFG(client))

class LFG(commands.Cog):
    """LFG related commands"""
    def __init__(self, client: LittleLightClient):
        self.client = client
        self.config = client.config
        self.destiny = client.destiny
        self.util = client.util

    # 1. Create search for gametype with a title
    # 2. All servers with the bot are alerted your search was created
    ## 2a. Search is appended to the top of the global list of d.lfg list
    # 3. A user may react to your search entry to signify interest
    ## 3a. Little Light messages you with the player that their profile
    ## 3b. You may react to accept/deny the player
    # 4. On accept, the player is invited to a group chat with you and the rest of the players in the party
    # 4a. You may invite them using their profile information in game

    @commands.group("lfg")
    async def lfg(self, ctx):
        """Look For Game"""

    @lfg.command("global")
    async def lfgGlobalCommand(self, ctx):
        """Sets the server's LFG preference to be global (all Little Light servers)"""

    @lfg.command("local")
    async def lfgLocalCommand(self, ctx):
        """Sets the server's LFG preference to be local (only your server)"""

    @lfg.command("list")
    async def lfgListCommand(self, ctx, activity_type: typing.Optional[int] = -1):
        """List all current open searches or all searches for given activity"""

    @lfg.command("search")
    async def lfgSearchCommand(self, ctx, players_needed: int, activity_type: int, *, title: str):
        """Search for a group for given activity"""